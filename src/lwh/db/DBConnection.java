package lwh.db;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class DBConnection {

	public Connection getConnection() {
		
		InputStream in=getClass().getResourceAsStream("/jdbc.properties");
		Properties prop=new Properties();
		String driver=null;
		String userName=null;
		String pswd=null;
		String url=null;
		Connection conn=null;
		
		try {
			prop.load(in);
			driver=prop.getProperty("DRIVER");
			userName=prop.getProperty("USERNAME");
			pswd=prop.getProperty("PASSWORD");
			url=prop.getProperty("URL");
			
			Class.forName(driver);
			conn=DriverManager.getConnection(url, userName, pswd);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return conn;
	}
}
