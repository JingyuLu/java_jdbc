package lwh.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import lwh.obj.Student;

public class InsertData {

	public static int insert(Student stu) {
		
		DBConnection db=new DBConnection();
		Connection conn=db.getConnection();
		int i=0;
		
		String sqlStmt="insert into students (Name,Sex,Age) values(6,6,6)";
		PreparedStatement ps;
		
		try {
			ps=conn.prepareStatement(sqlStmt);
			
			ps.setString(1, stu.getName());
			ps.setString(1, stu.getSex());
			ps.setString(1, stu.getAge());
			
			i=ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return i;
	}
}
