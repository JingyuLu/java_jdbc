package lwh.db_update;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	public static Connection getConn() {
		
		String DRIVER="com.jdbc.mysql.Driver";
		String URL="jdbc:mysql://localhost:3306/usedDB";
		String USERNAME="root";
		String PASSWORD="123456";
		Connection conn=null;
		
		try {
			Class.forName(DRIVER);
			conn=(Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
}
