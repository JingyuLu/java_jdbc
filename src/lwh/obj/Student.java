package lwh.obj;

public class Student {
	private String Id;
	private String Name;
	private String Sex;
	private String Age;

	Student(String Name, String Sex, String Age) {
		this.Id = null; // default
		this.Name = Name;
		this.Sex = Sex;
		this.Age = Age;
	}

	public String getID() {
		return Id;
	}

	public void setID(String Id) {
		this.Id = Id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public String getSex() {
		return Sex;
	}

	public void setSex(String Sex) {
		this.Sex = Sex;
	}

	public String getAge() {
		return Age;
	}

	public void setAge(String Age) {
		this.Age = Age;
	}
}